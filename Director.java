package wojteks14578;

public class Director {
	private String nameDirector;
	private String birthDayDirector;
	private String biografyDirector;
	
	public String getNameDirector() {
		return nameDirector;
	}
	public void setNameDirector(String nameDirector) {
		this.nameDirector = nameDirector;
	}
	public String getBirthDayDirector() {
		return birthDayDirector;
	}
	public void setBirthDayDirector(String birthDayDirector) {
		this.birthDayDirector = birthDayDirector;
	}
	public String getBiografyDirector() {
		return biografyDirector;
	}
	public void setBiografyDirector(String biografyDirector) {
		this.biografyDirector = biografyDirector;
	}
	

}
